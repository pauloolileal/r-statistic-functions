library(R6)

RegraDeTres <- R6Class("RegraDeTres",
  public = list(
    percent = NULL,
    totalPercent = 100.0,
	value = NULL,
	totalValue = NULL,
    initialize = function(percent = NULL, totalPercent = 100, value = NULL, totalValue = NULL) {
      self$percent <- percent
      self$value <- value
	  self$totalValue <- totalValue
      self$totalPercent <- totalPercent
    },
    calcular = function() {
		if(is.null(self$percent)){                     
			return((self$value * self$totalPercent) / self$totalValue)      
		}                                                
													  
		if(is.null(self$value)){                       
			return((self$percent * self$totalValue) / self$totalPercent)    
		}                                                
													  
		if(is.null(self$totalValue)){                  
			return((self$value * self$totalPercent) / self$percent)         
		}                                                
													  
		return ("Error")    
    }
  )
)