library(R6)

EstimativaPontual <- R6Class("EstimativaPontual",
  public = list(
    listaDeElementos = NULL,
    initialize = function(listaDeElementos = NULL) {
      self$listaDeElementos <- listaDeElementos
    },
	media = function() { 
		sum(self$listaDeElementos)/length(self$listaDeElementos) 
	},
	variancia = function() { 
		total <- 0
		for(i in self$listaDeElementos){
			total <- total + (i - self$media())^2
		}		
		total / length(self$listaDeElementos) 
	},
	desvioPadrao = function() { sqrt(self$variancia()) },
    proporcao = function(x) {
		x / length(self$listaDeElementos)
	}
  )
)
