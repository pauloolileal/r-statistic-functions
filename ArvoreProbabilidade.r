# Baseado no código: https://daranzolin.github.io/2018-01-07-probability-trees/

library(DiagrammeR)

ArvoreProbabilidade <- function(probabilidade, probPositivo, probNegativo) {
  
  if (!all(c(probabilidade, probPositivo, probNegativo) > 0) && !all(c(probabilidade, probPositivo, probNegativo) < 1)) {
    stop("O valor das probabilidades devem estar entre 0 e 1",
         call. = FALSE)
  }
  c_probabilidade <- 1 - probabilidade
  c_tp <- 1 - probPositivo
  c_tn <- 1 - probNegativo
  
  round4 <- purrr::partial(round, digits = 4)
  
  b1 <- round4(probabilidade * probPositivo)
  b2 <- round4(probabilidade * c_tp)
  b3 <- round4(c_probabilidade * c_tn)
  b4 <- round4(c_probabilidade * probNegativo)
  
  bp <-  round4(b1/(b1 + b3))
  
  labs <- c("X", probabilidade, c_probabilidade, probPositivo, c_tp, probNegativo, c_tn, b1, b2, b4, b3)
  
  tree <-
    create_graph() %>%
    add_n_nodes(
      n = 11,
      type = "path",
      label = labs,
      node_aes = node_aes(
        shape = "circle",
        height = 1,
        width = 1,
        x = c(0, 3, 3, 6, 6, 6, 6, 8, 8, 8, 8),
        y = c(0, 2, -2, 3, 1, -3, -1, 3, 1, -3, -1))) %>% 
    add_edge(
      from = 1,
      to = 2,
      edge_aes = edge_aes(
        label = "Probabilidade"
      )
    ) %>% 
    add_edge(
      from = 1, 
      to = 3,
      edge_aes = edge_aes(
        label = "Probabilidade complementar"
      )
    ) %>% 
    add_edge(
      from = 2,
      to = 4,
      edge_aes = edge_aes(
        label = "Verdadeiro positivo"
      )
    ) %>% 
    add_edge(
      from = 2,
      to = 5,
      edge_aes = edge_aes(
        label = "Falso negativo"
      )
    ) %>% 
    add_edge(
      from = 3,
      to = 7,
      edge_aes = edge_aes(
        label = "Falso positivo"
      )
    ) %>% 
    add_edge(
      from = 3,
      to = 6,
      edge_aes = edge_aes(
        label = "Verdadeiro negativo"
      )
    ) %>% 
    add_edge(
      from = 4,
      to = 8,
      edge_aes = edge_aes(
        label = "="
      )
    ) %>% 
    add_edge(
      from = 5,
      to = 9,
      edge_aes = edge_aes(
        label = "="
      )
    ) %>% 
    add_edge(
      from = 7,
      to = 11,
      edge_aes = edge_aes(
        label = "="
      )
    ) %>% 
    add_edge(
      from = 6,
      to = 10,
      edge_aes = edge_aes(
        label = "="
      )
    ) 
  message(glue::glue("The probability of having (probabilidade) after testing positive is {bp}"))
  print(render_graph(tree))
  invisible(tree)
}