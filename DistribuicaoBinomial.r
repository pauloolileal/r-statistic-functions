library(R6)

DistribuicaoBinomial <- R6Class("DistribuicaoBinomial",
  public = list(
    n = NULL,
    p = NULL,
	q = NULL,
    initialize = function(n = NULL, p = NULL) {
      self$n <- n
      self$p <- p
	  self$q <- (1 - p)
    },
	media = function() { self$n * self$p },
	variancia = function() { self$n * self$p * self$q},
	desvioPadrao = function() { sqrt(self$variancia()) },
	coeficienteBinomial = function(x) {
		factorial(self$n) / ((factorial(x)) * factorial(self$n - x))
	},
    calcular = function(x) {
		self$coeficienteBinomial(x) * self$p^x * (1 - self$p)^(self$n - x)
	},
	calcularIntervalo = function(positions) {
		total <- 0
		for(i in positions){
			total <- total + self$calcular(i)
		}
		total
	}
  )
)