library(R6)

source("TabelaTStudent.r")
source("TabelaDistribuicaoNormalPadrao.R")

ValorP <- R6Class("ValorP",
  public = list(
    media = NULL,
    desvioPadrao = NULL,
    n = NULL,
    tabelaDistNormal = NULL,
    tabelaTStudent = NULL,
    initialize = function(media = NULL, desvioPadrao = NULL, n = NULL) {
      self$media <- media
	  self$desvioPadrao <- desvioPadrao
      self$n <- n
      self$tabelaDistNormal <- TabelaDistribuicaoNormalPadrao$new()
      self$tabelaTStudent <- TabelaTStudent$new()
    },
	estatisticaTeste = function(x) {
		(x - self$media) / (self$desvioPadrao / sqrt(self$n))
	},
    devoRejeitar = function(x, nivelSignificancia = 0.05) {
        estatisticaTeste <- self$estatisticaTeste(x)
        valorSignificancia <- self$tabelaDistNormal$obterZ(nivelSignificancia)
        print(valorSignificancia)
        if(estatisticaTeste > valorSignificancia)
            return(TRUE)
        else 
            return(FALSE)
    },
    calcularUnilateralDireito = function(x, nivelSignificancia = 0.05, desvioPadraoConhecido = TRUE) {
        resposta <- 0
        estatisticaTeste <- self$estatisticaTeste(x)
		if(desvioPadraoConhecido){
            probabilidade <- self$tabelaDistNormal$obter(estatisticaTeste)
            if(estatisticaTeste > 0)
                resposta <- 0.5 - probabilidade
            else 
               resposta <- 0.5 + probabilidade         
        }else{
            grauLiberdade <- self$n - 1
            probabilidade <- as.numeric( sub("\\,", ".", self$tabelaTStudent$obterZ(estatisticaTeste)[2]) )
            if(estatisticaTeste > 0)
                resposta <- probabilidade
            else 
                resposta <- 1 - probabilidade
        }
        resposta
	},
    calcularBilateral = function(x, desvioPadraoConhecido = TRUE) {
        resposta <- 0
        estatisticaTeste <- self$estatisticaTeste(x)
		if(desvioPadraoConhecido){
            probabilidade <- self$tabelaDistNormal$obter(estatisticaTeste)
            resposta <- (0.5 - probabilidade) * 2
        }else{
            grauLiberdade <- self$n - 1
            probabilidade <- as.numeric( sub("\\,", ".", self$tabelaTStudent$obterZ(estatisticaTeste)[2]) )
            resposta <- probabilidade * 2
        }
        resposta
	},
    calcularUnilateralEsquerdo = function(x, desvioPadraoConhecido = TRUE) {
        resposta <- 0
        estatisticaTeste <- self$estatisticaTeste(x)
		if(desvioPadraoConhecido){
            probabilidade <- self$tabelaDistNormal$obter(estatisticaTeste)
            if(estatisticaTeste > 0)
                resposta <- 0.5 + probabilidade         
            else 
                resposta <- 0.5 - probabilidade
        }else{
            grauLiberdade <- self$n - 1
            probabilidade <- as.numeric( sub("\\,", ".", self$tabelaTStudent$obterZ(estatisticaTeste)[2]) )
            if(estatisticaTeste > 0)
                resposta <- 1 - probabilidade
            else 
                resposta <- probabilidade
        }
        resposta
	}
  )
)