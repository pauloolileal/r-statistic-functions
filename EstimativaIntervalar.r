library(R6)

source("TabelaTStudent.r")
source("TabelaDistribuicaoNormalPadrao.R")

EstimativaIntervalar <- R6Class("EstimativaIntervalar",
  public = list(
    n = NULL,
    initialize = function(n = NULL) {
      self$n <- n
    },
	grauConfianca = function(valor) {
		tabela <- TabelaDistribuicaoNormalPadrao$new()
		pos <- valor/2
		tabela$obterZ(pos)
	},
    grauLiberdade = function(intervaloConfianca) {
		tabela <- TabelaTStudent$new()
		tabela$obterGrau(intervaloConfianca, self$n)
	},
    margemErro = function(grau, desvioPadrao, desvioPopulacional = TRUE) {
        if(desvioPopulacional){
            margemErro <- self$grauConfianca(grau) * (desvioPadrao / sqrt(self$n))
        }else {
            margemErro <- self$grauLiberdade(grau) * (desvioPadrao / sqrt(self$n))
        }
        margemErro
    },
	media = function(grau, mediaAmostral, desvioPadrao, desvioPopulacional = TRUE, margemErro = NULL) {
        if(is.null(margemErro))
            margemErro <- self$margemErro(grau, desvioPadrao, desvioPopulacional)
		c(mediaAmostral - margemErro, mediaAmostral + margemErro)
	},
    proporcao = function(grau, p = 0.5, margemErro = NULL) {
        if(is.null(margemErro))
		    margemErro <- self$grauConfianca(grau) * (sqrt(p * (1 - p)/ self$n) )
		c(p - margemErro, p + margemErro)
	},
    tamanhoAmostraPorProporcao = function(erro, grau, proporcao = 0.5) {
        n <- (proporcao * (1 - proporcao))/((erro ^ 2) / (self$grauConfianca(grau) ^ 2))
        n
    },
    tamanhoAmostraPorVariancia= function(erro, grau, variancia) {
        n <- ((sqrt(variancia) * self$grauConfianca(grau)) / erro) ^ 2
        n
    },
    tamanhoAmostraPorDesvio = function(erro, grau, desvioPadrao) {
        n <- ((desvioPadrao * self$grauConfianca(grau)) / erro) ^ 2
        n
    }
  )
)
