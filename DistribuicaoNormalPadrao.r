source("TabelaDistribuicaoNormalPadrao.r")

DistribuicaoNormalPadrao <- R6Class("DistribuicaoNormalPadrao",
  public = list(
    media = NULL,
    desvioPadrao = NULL,
    tabela = NULL,
    initialize = function(media = NULL, desvioPadrao = NULL) {
      self$media <- media
      self$desvioPadrao <- desvioPadrao
      self$tabela <- TabelaDistribuicaoNormalPadrao$new()
    },
	  variancia = function() {
		  self$desvioPadrao ^ 2
	  },
    calcularZ = function(x) {
		  (x - self$media) / self$desvioPadrao
	  },
    calcularX = function(probabilidade, negativo = FALSE) {
      z <- self$tabela$obterZ(probabilidade)
      if(negativo)
        z <- z * -1
      (z * self$desvioPadrao) + self$media
	  },
    calcular = function(x) { 
      z <- self$calcularZ(x)
      z <- round(z, 2)    
      self$tabela$obter(z)
    },
    calcularAcima = function(x) { 
      z <- self$calcularZ(x)
      z <- round(z, 2)    
      valor <- self$tabela$obter(z)
      
      if(z > 0)
        valor <- 0.5 - valor
      else
        valor <- 0.5 + valor

      valor
    },
    calcularAbaixo = function(x) { 
      z <- self$calcularZ(x)
      z <- round(z, 2)    
      valor <- self$tabela$obter(z)
      
      if(z > 0)
        valor <- 0.5 + valor
      else
        valor <- 0.5 - valor

      valor
    },
    calcularEntre = function(x1, x2){
      z1 <- self$calcular(x1)
      z2 <- self$calcular(x2)
      z1 + z2
    },
    tamanhoAmostra = function(z) {
      (self$desvioPadrao * z) + self$media
    }
  )
)