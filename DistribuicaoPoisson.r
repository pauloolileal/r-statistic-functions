library(R6)

DistribuicaoPoisson <- R6Class("DistribuicaoPoisson",
  public = list(
    media = NULL,
    initialize = function(media = NULL) {
      self$media <- media
    },
	variancia = function() { self$media },
	desvioPadrao = function() { sqrt(self$variancia()) },
    calcular = function(x) {
		( (exp(1) ^ (-1 * self$media)) * (self$media ^ x) )/( factorial(x) )
	},
	calcularIntervalo = function(positions) {
		total <- 0
		for(i in positions){
			total <- total + self$calcular(i)
		}
		total
	}
  )
)